/*********************************************************************
 * SMARTBOARDV3
 *
 * This is the code for the SmartBoardV3 controller.
 *
 * Major Updates:
 * 2021-05-30
 * - Added basic structure.
 * - Maybe this peice of software should be called "bridge", as it really
 *   shouldn't be doing too much on its own. This justs provides an access
 *   point into the mesh network for the pi.
 * 2021-05-29
 *  - Added serial communications.
 *  - Implemented Serial code 1, Return Node List.
 * 2021-05-17
 * - Updated to State Machine.
 *
 * Author - Alex Hansen
 * Date   - 2021-05-17
 *********************************************************************/
 
 /*********************************************************************
 * Settings
 *********************************************************************/

#define BAUD 115200
#define VERSION 20210604
//#define DEBUG
//#define ECHO

/*********************************************************************
 * Device Role
 *********************************************************************/

#define ROLE_PIECE   0
#define ROLE_GRID    1
#define ROLE_CONTROL 2

#define ROLE ROLE_CONTROL

/*********************************************************************
 * WIFI transmit power
 * See:
 *  https://gitlab.com/painlessMesh/painlessMesh/-/issues/403  
 *  https://github.com/espressif/esp-idf/blob/master/components/esp_wifi/include/esp_wifi.h
 *  Mapping Table {Power, max_tx_power} = {{8, 2}, {20, 5}, {28, 7}, {34, 8}, {44, 11}, {52, 13}, {56, 14}, {60, 15}, {66, 16}, {72, 18}, {80, 20}}.
 *********************************************************************/

#include "esp_wifi.h"
#define  TX_PW_VALUE 8

/*********************************************************************
 * PAINLESS MESH
 *********************************************************************/

#include <painlessMesh.h>
#define   MESH_SSID       "whateverYouLike"
#define   MESH_PASSWORD   "somethingSneaky"
#define   MESH_PORT       5555

painlessMesh  mesh;
SimpleList<uint32_t> nodes;

// Prototypes
void receivedCallback(uint32_t from, String & msg);
void newConnectionCallback(uint32_t nodeId);
void changedConnectionCallback(); 
void nodeTimeAdjustedCallback(int32_t offset); 
void delayReceivedCallback(uint32_t from, int32_t delay);

/*********************************************************************
 * Scheduler
 *********************************************************************/
Scheduler     userScheduler; // to control your personal task

/*********************************************************************
 * State Machines
 *********************************************************************/
byte current_state = 0;
byte new_state = 0;

// Prototype the switch state function.
void switch_state( int new_state );

//Define the states, and any variables that the state uses:
#define STATE_STARTUP 1

#define STATE_LISTENING 2
String incoming_message = "";
#define MESSAGE_END '\n'

#define STATE_DECODE_MESSAGE 3
DynamicJsonDocument json_message(1024);

/*********************************************************************
 * Command defines
 *********************************************************************/
#define COMMAND_GET_NODE_LIST 0
#define COMMAND_SEND_TO_NODE 1
#define COMMAND_SEND_BROADCAST 2

/*********************************************************************
 * Command Functions
 *********************************************************************/
void do_send_node_list( void ) {
  DynamicJsonDocument NodeListToSend(1024);

  SimpleList<uint32_t> nodes = mesh.getNodeList();
  SimpleList<uint32_t>::iterator node = nodes.begin();
  int node_count = 0;

  while (node != nodes.end()) {
    NodeListToSend["node list"][node_count] = *node;

    node++;
    node_count++;
  }
  NodeListToSend["node count"] = node_count;

  serializeJson(NodeListToSend, Serial);
}

void do_send_to_node( void ) {
  /* If we receive a "Send to node" command, use the pre parsed JSON data to send the message to the destination node.
  Example commands for debugging:
  {"command":1,"destination":2824897929,"message":{"command":20}}
  {"command":1,"destination":2824900857,"message":{"command":3}}
  {"command":1,"destination":2824897929,"message":{"command":12, "color":63519}}
  */
#ifdef DEBUG
  Serial.println("In do_send_to_node");
#endif  
  uint32_t destination = json_message["destination"];
  String message = json_message["message"];

#ifdef DEBUG
  Serial.print("Sending  to:"); Serial.println(destination);
  Serial.print("Sending msg:"); Serial.println(message);
#endif  

  mesh.sendSingle(destination, message);
}

void do_send_broadcast( void ) {
  /* If we receive a "Send broadcasr" command, use the pre-parsed JSON data to send the message to all the connected nodes.
  Example commands for debugging:
  {"command":2,"message":{"command":0}}
  */  
#ifdef DEBUG  
  Serial.println("In do_send_broadcast");
#endif  
  String message = json_message["message"];
#ifdef DEBUG  
  Serial.print("Sending msg:"); Serial.println(message);
#endif  

  mesh.sendBroadcast(message);    
}

/*********************************************************************
 * User Tasks
 *********************************************************************/
void serial_Listener( void ) {
  /* Task to listen to the incoming serial stream and setup interactions with it */

  if ( Serial.available() > 0 ) {
    char incoming = Serial.read();
    
    if ( incoming == MESSAGE_END ) {
#ifdef DEBUG      
      Serial.println("Message received");
#endif      
      switch_state( STATE_DECODE_MESSAGE );
    }
    else {
#ifdef ECHO      
      Serial.write(incoming); // Manual echo?
#endif      
      incoming_message.concat(incoming);
    }
  }
}
//Task serial_Listener_Task( TASK_MILLISECOND * 10, TASK_FOREVER, &serial_Listener );
Task serial_Listener_Task( TASK_SECOND * 0.01, TASK_FOREVER, &serial_Listener );

void decode_message( void ) {
#ifdef DEBUG  
  Serial.print("decoding: "); Serial.println( incoming_message );
#endif  
  deserializeJson(json_message, incoming_message);
  incoming_message = "";

  int command = json_message["command"];

#ifdef DEBUG
  Serial.print("Command is:"); Serial.println(command);
#endif  
  switch ( command ) {
    case COMMAND_GET_NODE_LIST : { do_send_node_list(); switch_state( STATE_LISTENING ); break; }
    case COMMAND_SEND_TO_NODE : { do_send_to_node(); switch_state( STATE_LISTENING ); break; }
    case COMMAND_SEND_BROADCAST : { do_send_broadcast(); switch_state( STATE_LISTENING ); break; }    
    default : switch_state( STATE_LISTENING );
  }

}
Task decode_message_Task( TASK_SECOND * 0.01, TASK_FOREVER, &decode_message );

/*********************************************************************
 * Setups
 *********************************************************************/
void mesh_setup() {
  mesh.setDebugMsgTypes(ERROR | DEBUG);  // set before init() so that you can see error messages

  mesh.init(MESH_SSID, MESH_PASSWORD, &userScheduler, MESH_PORT);

  esp_wifi_set_max_tx_power(TX_PW_VALUE);

  // Bridge node, should (in most cases) be a root node. See [the wiki](https://gitlab.com/painlessMesh/painlessMesh/wikis/Possible-challenges-in-mesh-formation) for some background
  mesh.setRoot(true);
  // This node and all other nodes should ideally know the mesh contains a root, so call this on all nodes
  mesh.setContainsRoot(true);

#ifdef DEBUG
  Serial.print("Mesh Node ID = \""); Serial.print( mesh.getNodeId()); Serial.println("\"");
#endif  

  mesh.onReceive(&receivedCallback);
  mesh.onNewConnection(&newConnectionCallback);
  mesh.onChangedConnections(&changedConnectionCallback);
  mesh.onNodeTimeAdjusted(&nodeTimeAdjustedCallback);
  mesh.onNodeDelayReceived(&delayReceivedCallback); 
}

void task_setup() {
  userScheduler.addTask( serial_Listener_Task );
  userScheduler.addTask( decode_message_Task );
}

void setup() {
  // Serial setup
  Serial.begin( BAUD );

  // MESH setup
  mesh_setup();

  // Task Setup
  task_setup();

  // Set our inital state:
  switch_state( STATE_STARTUP );
}

/*********************************************************************
 * Loop
 *********************************************************************/
void loop() {
  mesh.update();
}

/*********************************************************************
 * STATE MACHINE
 *********************************************************************/
void switch_state( int new_state ) {
  /*
  Switch from one state to another. This will short circuit is the state doesn't really change.
  */

  //Check to see if we've changed states, and if not return
  if ( current_state == new_state ) {
#ifdef DEBUG    
    Serial.println("Call to change state to current state!");
#endif    
    return; }

#ifdef DEBUG
  Serial.print("Changing state from "); Serial.print( current_state ); Serial.print(" to "); Serial.println( new_state );
#endif  
  
  //Stop all tasks, the state list below should enable them as needed.
  serial_Listener_Task.disable();
  decode_message_Task.disable();  

  switch ( new_state ) {
    case STATE_STARTUP : { switch_state( STATE_LISTENING ); break; }
    case STATE_LISTENING : { serial_Listener_Task.enable(); break; }
    case STATE_DECODE_MESSAGE : { decode_message_Task.enable(); break; }
  }

  // Now update our current state to the new state
  current_state = new_state;
}

/*********************************************************************
 * Mesh Call Backs - Prototypes above.
 *********************************************************************/
void receivedCallback(uint32_t from, String & msg) {
  // Since we are a simple bridge, pass the message from the Node back to the Pi  
  Serial.printf("{\"node\":%u, \"message\":%s}\n", from, msg.c_str());
}
void newConnectionCallback(uint32_t nodeId) {
//  Serial.printf("--> Mesh: New Connection, nodeId = %u\n", nodeId);
//  Serial.printf("--> Mesh: New Connection, %s\n", mesh.subConnectionJson(true).c_str());  
}
void changedConnectionCallback() {
//  Serial.printf("--> Mesh: Changed connections\n");  
}
void nodeTimeAdjustedCallback(int32_t offset) {
//  Serial.printf("--> Mesh: Adjusted time %u. Offset = %d\n", mesh.getNodeTime(), offset);  
}
void delayReceivedCallback(uint32_t from, int32_t delay) {
//  Serial.printf("--> Mesh: Delay to node %u is %d us\n", from, delay);  
}